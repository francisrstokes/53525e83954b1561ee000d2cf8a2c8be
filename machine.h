#ifndef INC_MACHINE_H
#define INC_MACHINE_H

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

typedef void (*state_change_fn)(uint32_t, uint32_t);
typedef bool (*state_condition_fn)(uint32_t);

typedef struct state_transition_t {
  const uint32_t from_state;
  const uint32_t to_state;
  const state_condition_fn condition;
} state_transition_t;

typedef struct state_machine_t {
  state_transition_t* transitions;
  uint32_t num_transitions;
  state_change_fn on_state_change;
  uint32_t* state_index;
} state_machine_t;

void state_machine_step(state_machine_t* descriptor);

#endif // INC_MACHINE_H

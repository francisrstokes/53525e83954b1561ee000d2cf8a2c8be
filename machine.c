#include "machine.h"

void state_machine_step(state_machine_t* descriptor) {
  state_transition_t* transition;
  uint32_t current_state = *descriptor->state_index;

  for (uint32_t i = 0; i < descriptor->num_transitions; i++) {
    transition = &descriptor->transitions[i];
    if ((transition->from_state == current_state) && (transition->condition(current_state))) {
      descriptor->on_state_change(current_state, transition->to_state);
      *descriptor->state_index = transition->to_state;
      return;
    }
  }
}
